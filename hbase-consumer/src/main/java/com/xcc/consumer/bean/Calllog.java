package com.xcc.consumer.bean;

import com.xcc.anno.ColumnRef;
import com.xcc.anno.RowkeyRef;
import com.xcc.anno.TableRef;

@TableRef("ct:call")
public class Calllog {

    @RowkeyRef
    private String rowKey;
    @ColumnRef
    private String call1;
    @ColumnRef
    private String call2;
    @ColumnRef
    private String calltime;
    @ColumnRef
    private String duration;
    @ColumnRef
    private Integer flag;

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Calllog(String rowKey, String call1, String call2, String calltime, String duration, Integer flag) {
        this.rowKey = rowKey;
        this.call1 = call1;
        this.call2 = call2;
        this.calltime = calltime;
        this.duration = duration;
        this.flag = flag;
    }

    public Calllog(String data) {
        String[] dataArr = data.split("\t");
        this.call1 = dataArr[0];
        this.call2 = dataArr[1];
        this.calltime = dataArr[2];
        this.duration = dataArr[3];
    }

    public String getRowKey() {
        return rowKey;
    }

    public void setRowKey(String rowKey) {
        this.rowKey = rowKey;
    }

    public String getCall1() {
        return call1;
    }

    public void setCall1(String call1) {
        this.call1 = call1;
    }

    public String getCall2() {
        return call2;
    }

    public void setCall2(String call2) {
        this.call2 = call2;
    }

    public String getCalltime() {
        return calltime;
    }

    public void setCalltime(String calltime) {
        this.calltime = calltime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
