package com.xcc.consumer;

import com.xcc.consumer.kafka.ConsumerAndHbaseProducer;


public class ConsumerBootStrap {

    public static void main(String[] args) throws Exception {
        ConsumerAndHbaseProducer hbaseProducer = new ConsumerAndHbaseProducer();

//        hbaseProducer.getHbaseTable("19342117869","201901","201905");

        hbaseProducer.consumer();
    }
}
