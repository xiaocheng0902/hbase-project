package com.xcc.mr.out;

import com.xcc.mr.jdbc.MyJDBCUtil;
import com.xcc.mr.mapper.MapperBean;
import com.xcc.mr.redis.MyContants;
import com.xcc.mr.reducer.ReducerBean;
import jdk.nashorn.internal.objects.NativeNumber;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MyRecordWriter extends RecordWriter<MapperBean, ReducerBean> {

    private Jedis jedis = null;
    private Connection connection = null;

    public MyRecordWriter() {
        jedis = new Jedis("hadoop102", 6379);
        connection = MyJDBCUtil.getConnection();
    }

    public void write(MapperBean mapperBean, ReducerBean reducerBean) throws IOException, InterruptedException {
        PreparedStatement ps = null;
        try {
            String tel = mapperBean.getTel();
            String calltime = mapperBean.getCalltime();
            Long count = reducerBean.getCount();
            Long duration = reducerBean.getDuration();
            ps = connection.prepareStatement("insert into ct_result(user_id,date_id,sum_count,sum_duration) values (?,?,?,?)");
            String dataid = jedis.hget(MyContants.DATEKEY, calltime);
//            System.out.println(calltime + "===" + dataid);
            String userid = jedis.hget(MyContants.USERKEY, tel);
            ps.setInt(1, Integer.parseInt(userid));
            ps.setInt(2, Integer.parseInt(dataid));
            ps.setInt(3, Integer.parseInt(count.toString()));
            ps.setInt(4, Integer.parseInt(duration.toString()));

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void close(TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {
        if (jedis != null) {
            jedis.close();
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
