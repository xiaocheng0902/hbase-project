package com.xcc.mr.jdbc;

import com.xcc.mr.redis.MyContants;

import java.sql.Connection;
import java.sql.DriverManager;

public class MyJDBCUtil {

    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName(MyContants.MYSQL_DRIVER_CLASS);
            connection = DriverManager.getConnection(MyContants.MYSQL_URL, MyContants.MYSQL_USERNAME, MyContants.MYSQL_PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

}
