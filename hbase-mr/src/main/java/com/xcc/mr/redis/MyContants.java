package com.xcc.mr.redis;

public interface MyContants {

    String DATEKEY = "date:2019";
    String USERKEY = "user:key";

    //redis
    String HOSTNAME = "hadoop102";
    Integer PORT = 6379;

    //mysql
    static final String MYSQL_DRIVER_CLASS = "com.mysql.jdbc.Driver";
    static final String MYSQL_URL = "jdbc:mysql://hadoop102:3306/ct_call?useUnicode=true&characterEncoding=UTF-8";
    static final String MYSQL_USERNAME = "root";
    static final String MYSQL_PASSWORD = "123456";

}
