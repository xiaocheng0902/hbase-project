package com.xcc.mr.redis;

import com.xcc.mr.jdbc.MyJDBCUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import redis.clients.jedis.Jedis;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

public class RedisDataStart {

    public static void main(String[] args) throws SQLException {
        Jedis jedis = new Jedis("hadoop102", 6379);
        jedis.del(MyContants.DATEKEY);
        jedis.del(MyContants.USERKEY);
        Connection connection = MyJDBCUtil.getConnection();
        PreparedStatement dateps = connection.prepareStatement("select * from ct_date");
        ResultSet dataResult = dateps.executeQuery();
        while (dataResult.next()) {
            int id = dataResult.getInt("id");
            String year = dataResult.getString("year");
            String month = dataResult.getString("month");
            if (month != null && month.length() ==1) {
                month = "0" + month;
            }
            String day = dataResult.getString("day");
            if (day != null && day.length() ==1) {
                day = "0" + day;
            }
            System.out.println(id + "===" + year + ":::" + month + ":::" + day);
            jedis.hset(MyContants.DATEKEY, year + month + day, id + "");
        }
        dataResult.close();

        PreparedStatement userps = connection.prepareStatement("select * from ct_user");
        ResultSet userResult = userps.executeQuery();
        while (userResult.next()) {
            int id = userResult.getInt("id");
            String tel = userResult.getString("tel");
//            String name = userResult.getString("name");
            jedis.hset(MyContants.USERKEY, tel, id+"");
        }
        userps.close();

        connection.close();
        jedis.close();

    }
}
