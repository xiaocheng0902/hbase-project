package com.xcc.mr.mapper;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class MapperBean implements WritableComparable {

    private String tel;
    private String calltime;

    public MapperBean(String tel, String calltime) {
        this.tel = tel;
        this.calltime = calltime;
    }

    public MapperBean() {

    }

    public int compareTo(Object o) {
        MapperBean mapperBean = (MapperBean) o;
        int result = tel.compareTo(mapperBean.getTel());
        if (result == 0) {
            return calltime.compareTo(mapperBean.getCalltime());
        }
        return result;
    }

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(tel);
        dataOutput.writeUTF(calltime);
    }

    public void readFields(DataInput dataInput) throws IOException {
        tel = dataInput.readUTF();
        calltime = dataInput.readUTF();
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCalltime() {
        return calltime;
    }

    public void setCalltime(String calltime) {
        this.calltime = calltime;
    }
}
