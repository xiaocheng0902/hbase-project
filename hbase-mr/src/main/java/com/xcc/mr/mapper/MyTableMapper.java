package com.xcc.mr.mapper;

import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;

import java.io.IOException;
import java.util.List;

public class MyTableMapper extends TableMapper<MapperBean, Text> {

    @Override
    protected void map(ImmutableBytesWritable key, Result value, Context context) throws IOException, InterruptedException {
        String rowkey = Bytes.toString(key.get());
        //3_13319935953_20191019045414_15884588694_0321_1
        String[] arr = rowkey.split("_");
        String time = arr[2];
        MapperBean yearBean = new MapperBean(arr[1],time.substring(0,4));
        MapperBean monthBean = new MapperBean(arr[1],time.substring(0,6));
        MapperBean dayBean = new MapperBean(arr[1],time.substring(0,8));
        context.write(yearBean, new Text(arr[4]));
        context.write(monthBean, new Text(arr[4]));
        context.write(dayBean, new Text(arr[4]));
    }
}
