package com.xcc.mr.tool;

import com.xcc.constant.CommonConstant;
import com.xcc.constant.Names;
import com.xcc.mr.mapper.MapperBean;
import com.xcc.mr.mapper.MyTableMapper;
import com.xcc.mr.out.MyOutputFormat;
import com.xcc.mr.reducer.MyReducer;
import com.xcc.mr.reducer.ReducerBean;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;

public class MyTool implements Tool {
    private Configuration conf;
    public int run(String[] strings) throws Exception {
        Job job = Job.getInstance(conf);

        Scan scan = new Scan();
        TableMapReduceUtil.initTableMapperJob(
                Names.TABLE.getValue(),
                scan,
                MyTableMapper.class,
                MapperBean.class, //mapper的输出key
                Text.class, //mapper的输出value
                job
        );

        job.setReducerClass(MyReducer.class);
        job.setOutputFormatClass(MyOutputFormat.class);
        job.setJarByClass(MyTool.class);
        job.setOutputKeyClass(MapperBean.class);
        job.setOutputValueClass(ReducerBean.class);

        boolean b = job.waitForCompletion(true);
        return b ? 0:1;
    }

    public void setConf(Configuration configuration) {
        this.conf = HBaseConfiguration.create();
    }

    public Configuration getConf() {
        return this.conf;
    }
}
