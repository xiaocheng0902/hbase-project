package com.xcc.mr.reducer;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class ReducerBean implements Writable {

    private Long count;
    private Long duration;

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeLong(count);
        dataOutput.writeLong(duration);
    }

    public void readFields(DataInput dataInput) throws IOException {
        count = dataInput.readLong();
        duration = dataInput.readLong();
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }
}
