package com.xcc.mr.reducer;

import com.xcc.mr.mapper.MapperBean;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class MyReducer extends Reducer<MapperBean, Text,MapperBean,ReducerBean> {

    @Override
    protected void reduce(MapperBean key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        ReducerBean reducerBean = new ReducerBean();
        long count = 0L;
        long duration = 0L;
        for (Text value : values) {
            count++;
            duration += Long.valueOf(value.toString());
        }
        reducerBean.setCount(count);
        reducerBean.setDuration(duration);
        context.write(key,reducerBean);
    }
}
