package com.xcc.mr;

import com.xcc.mr.tool.MyTool;
import org.apache.hadoop.util.ToolRunner;

public class MRAppStart {

    public static void main(String[] args) throws Exception {
        ToolRunner.run(new MyTool(), args);
    }

}
