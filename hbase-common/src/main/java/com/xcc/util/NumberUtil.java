package com.xcc.util;

import java.text.DecimalFormat;

public class NumberUtil {

    public static String format(int num,int length) {
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= length; i++) {
            sb.append("0");
        }
        DecimalFormat decimalFormat = new DecimalFormat(sb.toString());
        return decimalFormat.format(num);
    }

    public static void main(String[] args) {
        String random = format(10, 5);
        System.out.println(random);
    }
}
