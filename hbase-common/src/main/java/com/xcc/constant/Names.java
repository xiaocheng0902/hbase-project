package com.xcc.constant;

import com.xcc.bean.Val;

public enum Names implements Val {
    NAMESPACE("ct"),
    TOPIC("calllog"),
    TABLE("ct:call"),
    CALLER("caller"),
    CALLEE("callee"),
    FAMILY("info");

    private String value;
    Names(String calllog) {
        value = calllog;
    }

    public String getValue() {
        return value;
    }

    public Object getVal() {
        return null;
    }

}
