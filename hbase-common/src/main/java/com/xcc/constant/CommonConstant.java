package com.xcc.constant;


import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion;

public interface CommonConstant {
    //分区数
    public final static Integer REGION_COUNT = 4;

    //主叫
    public final static Integer FLAG_TURE = 1;

    //被叫
    public final static Integer FLAG_FLASE = 0;

}
