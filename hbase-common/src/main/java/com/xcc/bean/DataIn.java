package com.xcc.bean;

import java.io.Closeable;
import java.util.List;

public interface DataIn extends Closeable {

    public <T extends Data> List<T> read(Class<T> clazz);

    public void setPath(String path);

}
