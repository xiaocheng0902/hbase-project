package com.xcc.bean;

import java.io.Closeable;

public interface Producer extends Closeable {

    public void setDataIn(DataIn in) ;

    public void setDataOut(DataOut out);

    public void produce();
}
