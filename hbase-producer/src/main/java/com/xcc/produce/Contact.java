package com.xcc.produce;

import com.xcc.bean.Data;

public class Contact extends Data {

    private String tel;
    private String name;

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVal(String content) {
        String[] split = content.split("\t");
        setTel(split[0]);
        setName(split[1]);
    }

    public Object getVal() {
        return null;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "tel='" + tel + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
