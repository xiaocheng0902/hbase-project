package com.xcc.produce;

import com.xcc.bean.DataOut;

import java.io.*;

public class FileLocalDataOut implements DataOut {
    private PrintWriter writer;

    public FileLocalDataOut(String path) {
        setPath(path);
    }

    public void setPath(String path) {
        try {
            writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(path)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void write(Object obj) {
        writer.println(obj);
        writer.flush();
    }

    public void close() throws IOException {
        if (writer != null) {
            writer.close();
        }
    }
}
