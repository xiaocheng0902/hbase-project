package com.xcc.produce;

import com.xcc.bean.Data;
import com.xcc.bean.DataIn;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileLocalDataIn implements DataIn {

    private BufferedReader reader;

    public FileLocalDataIn(String path) {
        setPath(path);
    }

    public <T extends Data> List<T> read(Class<T> clazz) {
        List<T> ts = new ArrayList<T>();
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                T t = clazz.newInstance();
                t.setVal(line);
                ts.add(t);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ts;
    }

    public void setPath(String path) {
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(path),"UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close() throws IOException {
        if (reader != null) {
            reader.close();
        }
    }
}
