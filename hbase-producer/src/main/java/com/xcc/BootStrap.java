package com.xcc;

import com.xcc.produce.FileLocalDataIn;
import com.xcc.produce.FileLocalDataOut;
import com.xcc.produce.MyProducer;

import java.io.IOException;

public class BootStrap {

    public static void main(String[] args) throws Exception {
        if (args == null || args.length < 2) {
            System.out.println("请输出至少两个参数");
            System.exit(1);
        }

        MyProducer myProducer = new MyProducer();

//        myProducer.setDataIn(new FileLocalDataIn("F:\\学习\\2020\\22.尚硅谷大数据技术之电信客服综合案例\\contact.log"));
//        myProducer.setDataOut(new FileLocalDataOut("F:\\学习\\2020\\22.尚硅谷大数据技术之电信客服综合案例\\call.log"));
        myProducer.setDataIn(new FileLocalDataIn(args[0]));
        myProducer.setDataOut(new FileLocalDataOut(args[1]));


        /*new Thread(() -> {
            myProducer.produce();
        }, "bbb").start();

        //10秒之后关闭开关
        new Thread(() -> {
            try {
                Thread.sleep(2000);
                myProducer.flagClose();
                myProducer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, "aaa").start();*/

        myProducer.produce();
        myProducer.close();
    }

}
